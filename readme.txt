There exists two images (or canvas) with width W > 0 and height H > 0.
e.g. imageA = {W: 100, H: 300} and imageB = {W: 150, H: 600}.

imageB needs fit into imageA by 2 strategies, contain and cover. Upsizing imageB is not allowed. Images must keep aspect ratio.
    Contain: fits imageB fully so that not piece of imageB is outside imageA. imageB can be downsized but not upsized.
    Cover: fits imageA as much as possible, imageB an have parts outside imageA, imageB cannot be upsized.

All calculation are only Math based, no real image resizing wanted.

possible inputs:
 - imageA => W: 180, H: 250
 - imageB => W: 360, H: 200

 - imageA => W: 180, H: 250
 - imageB => W: 100, H: 500
 
 - imageA => W: 180, H: 250
 - imageB => W: 150, H: 245

possible output:
 - imageB width: 500, height: 500
 - width: 500, height: 500
 - 500 * 500
Print out the calculated Width and Height of imageB for implementation Contain and Cover.

Duration: 55 minutes

Points are given for,
1 point for implementation works for Contain
1 point for implementation works for Cover
1 point for Object Oriented approach
1 point for Namespaces
1 point for SPL auto loading
2 points for Interfaces, where they make sense
3 points for Strategy design pattern usage
